// Main CLass
// entry point for out java program
// main class has 1 method inside, the "main method" => run code.

public class Main {
    public static void main(String[] args) {
        // statement that allows us to print the value of argument passed into its terminal
        System.out.println("Hello world!");
    }
}