package com.zuitt.example;

import java.util.Scanner;

public class S1A1 {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter your First Name: ");

        String firstName = userInput.nextLine();


        System.out.println("Enter your Last Name: ");
        String lastName = userInput.nextLine();


        System.out.println("Enter First Subject Grade: ");
        double firstSubject = userInput.nextDouble();
        System.out.println("Enter Second Subject Grade: ");
        double secondSubject = userInput.nextDouble();
        System.out.println("Enter Third Subject Grade: ");
        double thirdSubject = userInput.nextDouble();

        System.out.println("First Name: \n" + firstName );
        System.out.println("Last Name: \n" + lastName);
        System.out.println("First Subject Grade: \n" + firstSubject);
        System.out.println("Second Subject Grade: \n" + secondSubject);
        System.out.println("Third Subject Grade: \n" + thirdSubject);

        double average = (firstSubject + secondSubject + thirdSubject) / 3;


        System.out.println("Good Day, " + firstName + " " + lastName + ".\n" + "Your grade average is : " + Math.round(average));

    }
}
