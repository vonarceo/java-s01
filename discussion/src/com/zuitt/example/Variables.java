// a package in java is ised to group related classes, think of it as a folder in a file directory

// package creation follows reserve name notation for the naming convention
package com.zuitt.example;

public class Variables {

    public static void main(String[] args){
        // Variable
        // Synthax: dataType identifier;
        int age;
        char middleInitial;

        // variable declaration vs initialization;

        int x;
        int y = 0;

        // initialization after declaration

        x = 1;
        y = 5;

        System.out.println("The Value of y is " + y + " and the value of x is " + x);

        // primitive data types
                // pre defined within the java programming language which is used for single valued variables with limited capabilities
        // int - whole numbers values

        int wholeNumber = 100;
        System.out.println(wholeNumber);

        // long
        //L is added at the end of the number to be recognized as long

        long worldPopulation = 8000000000L;
        System.out.println(worldPopulation);

        // float

        float piFloat = 3.14159226539f;
        double piFloat2 = 3.141592226539;
        System.out.println(piFloat);
        System.out.println(piFloat2);

        //char - single character
        // uses single qoutes.

        char letter = 'a';
        System.out.println(letter);

        // boolean
        boolean isLove = true;
        boolean isTaken = false;

        System.out.println(isLove);
        System.out.println(isTaken);

        //constants
        // Java uses the "final" keyword to the variable's value that cannot be changed.
        // UPPERCASE ALL if final
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        // PRINCIPAL = 4000; -(not gonna work)

        // NON-primitive data type

            // also known as reference data type refer to instances or objects.
            // include methods

            // do not directly store the value of a variable, but rather remembers the reference to that variable

            // String
                // Stores a sequences or array of characters/
                // string are actually object that can we use method

        String userName = "JSmith";
        System.out.println(userName);
        int stringLength = userName.length();
        System.out.println(stringLength);








    }

}
